#!/bin/bash

# Create docker network if exist
chmod +x ../imob-network/create-network-dev
../imob-network/create-network-dev

# Deploy db
docker-compose -f ../imob-db/docker-compose-dev.yaml up -d

# Deploy discovery
cd ../imob-discovery
gradle jibDockerBuild
docker-compose -f docker-compose-dev.yaml up -d

# Deploy gateway
cd ../imob-gateway
gradle jibDockerBuild
docker-compose -f docker-compose-dev.yaml up -d

# Deploy drive
cd ../imob-drive
gradle jibDockerBuild
docker-compose -f docker-compose-dev.yaml up -d

# Deploy inventory
cd ../imob-inventory
gradle jibDockerBuild
docker-compose -f docker-compose-dev.yaml up -d

# Deploy frontend
docker-compose -f ../imob-frontend/docker-compose-dev.yaml up -d --build

docker ps
